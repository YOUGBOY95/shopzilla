import Head from 'next/head'
import Homepage from '../components/Home/Homepage'
import WhyPlants from '../components/Home/WhyPlants'
import Layout from '../components/Layout'


export default function Home() {
  return (
    <>
      <Head>
        <title>Accueil - SHOPZILLA</title>
       
      </Head>
    <div>
      <Layout>
          <Homepage />
          <WhyPlants />
      </Layout>
    </div>
      
    </>
  )
}
