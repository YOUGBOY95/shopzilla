import { Auth, getAuth } from 'firebase/auth';
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDKAd9OR14zGqI0YFYkGged0eAs0ifSlzc",
    authDomain: "shopzilla1-d05f3.firebaseapp.com",
    databaseURL: "https://shopzilla1-d05f3-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "shopzilla1-d05f3",
    storageBucket: "shopzilla1-d05f3.appspot.com",
    messagingSenderId: "982456396567",
    appId: "1:982456396567:web:102a90354f60b42bce2552",
    measurementId: "G-141E5R22H1"
}

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
export const auth = getAuth(app);
export const storage = getStorage(app);