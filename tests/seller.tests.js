import { signIn, signOutSeller, addPhotoToItem } from '../slices/sellerSlice';
import { sellerActions } from '../slices/sellerSlice';

jest.mock('../src/firebase-config', () => ({
  auth: {},
  db: {},
  storage: { ref: jest.fn(() => ({ child: jest.fn(() => ({ put: jest.fn(() => ({ getDownloadURL: jest.fn(() => 'mocked-url') })) })) })) },
}));

jest.mock('firebase/auth', () => ({
  signInWithEmailAndPassword: jest.fn(),
  signOut: jest.fn(),
}));

jest.mock('firebase/firestore', () => ({
  collection: jest.fn(),
  query: jest.fn(),
  where: jest.fn(),
  getDocs: jest.fn(),
}));

describe('Seller actions', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should dispatch SIGN_IN action on successful sign in', async () => {
    const userData = { uid: '123', email: 'test@example.com' };
    const dispatch = jest.fn();
    const signInMock = jest.fn().mockResolvedValue({ user: userData });
    require('firebase/auth').signInWithEmailAndPassword = signInMock;

    const docData = { data: () => userData, id: 'docId' };
    const getDocsMock = jest.fn().mockResolvedValueOnce([docData]);
    require('firebase/firestore').getDocs = getDocsMock;

    await signIn('test@example.com', 'password')(dispatch);

    expect(signInMock).toHaveBeenCalledWith(expect.anything(), 'test@example.com', 'password');
    expect(getDocsMock).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith(sellerActions.SIGN_IN({ ...userData, docId: 'docId' }));
  });

  it('should dispatch SET_ERROR action on sign in error', async () => {
    const signInMock = jest.fn().mockRejectedValue(new Error('Some error'));
    require('firebase/auth').signInWithEmailAndPassword = signInMock;
    const dispatch = jest.fn();

    await signIn('test@example.com', 'password')(dispatch);

    expect(signInMock).toHaveBeenCalledWith(expect.anything(), 'test@example.com', 'password');
    expect(dispatch).toHaveBeenCalledWith(sellerActions.SET_ERROR('Some error'));
  });

  it('should dispatch SIGN_OUT action on sign out', async () => {
    const signOutMock = jest.fn().mockResolvedValue();
    require('firebase/auth').signOut = signOutMock;
    const dispatch = jest.fn();

    await signOutSeller()(dispatch);

    expect(signOutMock).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith(sellerActions.SIGN_OUT());
  });

  it('should upload photo and return URL', async () => {
    const itemId = '123';
    const photoFile = { name: 'test.jpg' };
    const dispatch = jest.fn();

    const photoURL = await addPhotoToItem(itemId, photoFile)(dispatch);

    expect(photoURL).toBe('mocked-url');
  });
});
