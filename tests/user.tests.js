import { signIn, signOut } from '../slices/userSlice';
import { userActions } from '../slices/userSlice';

jest.mock('../src/firebase-config', () => ({
  auth: {},
  db: {},
}));

jest.mock('firebase/auth', () => ({
  signInWithEmailAndPassword: jest.fn(),
  signOut: jest.fn(),
}));

jest.mock('firebase/firestore', () => ({
  collection: jest.fn(),
  query: jest.fn(),
  where: jest.fn(),
  getDocs: jest.fn(),
}));

describe('User actions', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should dispatch SIGN_IN action on successful sign in', async () => {
    const userData = { uid: '123', email: 'test@example.com' };
    const dispatch = jest.fn();
    const signInMock = jest.fn().mockResolvedValue({ user: userData });
    require('firebase/auth').signInWithEmailAndPassword = signInMock;

    const docData = { data: () => userData, id: 'docId' };
    const getDocsMock = jest.fn().mockResolvedValueOnce([docData]);
    require('firebase/firestore').getDocs = getDocsMock;

    await signIn('test@example.com', 'password')(dispatch);

    expect(signInMock).toHaveBeenCalledWith(expect.anything(), 'test@example.com', 'password');
    expect(getDocsMock).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith(userActions.SIGN_IN({ ...userData, docId: 'docId' }));
  });

  it('should dispatch SET_ERROR action on sign in error', async () => {
    const signInMock = jest.fn().mockRejectedValue(new Error('Some error'));
    require('firebase/auth').signInWithEmailAndPassword = signInMock;
    const dispatch = jest.fn();

    await signIn('test@example.com', 'password')(dispatch);

    expect(signInMock).toHaveBeenCalledWith(expect.anything(), 'test@example.com', 'password');
    expect(dispatch).toHaveBeenCalledWith(userActions.SET_ERROR('Some error'));
  });

  it('should dispatch SIGN_OUT action on sign out', async () => {
    const signOutMock = jest.fn().mockResolvedValue();
    require('firebase/auth').signOut = signOutMock;
    const dispatch = jest.fn();

    await signOut()(dispatch);

    expect(signOutMock).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith(userActions.SIGN_OUT());
  });
});
