import React from 'react';
import { motion } from 'framer-motion';
import { useSelector, useDispatch } from 'react-redux';
import CartItem from './CartItem';
import { REMOVE_CART_ITEM } from '../slices/cartSlice';

function Cart({ setShow }) {
  const dispatch = useDispatch();
  const cartItems = useSelector(state => state.cart.cartItems);

  const removeFromCart = (id) => {
    dispatch(REMOVE_CART_ITEM(id));
  };

  return (
    <motion.div
      className="fixed w-full seconde:w-[570px] z-50 text-black bg-white top-0 right-0 min-h-screen shadow-lg pt-5 px-5 md:px-8"
      initial={{ x: '100%', opacity: 0 }} // déplace le composant hors de l'écran à droite
      animate={{ x: 0, opacity: 1 }} // anime le composant pour le déplacer vers la gauche et le rendre visible
      exit={{ x: '100%', opacity: 0 }} // anime le composant pour le déplacer hors de l'écran à droite lors de sa disparition
      transition={{ duration: 0.5 }} // durée de l'animation
    >
      <button onClick={setShow} className={`w-16 h-16  p-3 transition hover:bg-[#7c7c7c2c]`}>
        <img className="w-20" src='/icons/remove-icon-black.svg' alt="Close" />
      </button>

      <div className={`text-black flex flex-col gap-5 w-full mt-8 ${cartItems && cartItems.length > 3 && 'overflow-y-scroll'}  h-[65vh]`}>

        {cartItems.length ? cartItems.map(item => (
          <CartItem item={item} key={item.id} removeFromCart={removeFromCart} />
        )) : (
          <div className='text-black h-full flex flex-col gap-10 justify-center items-center text-3xl text-center'>
            Vous n'avez toujours rien ajouté au panier !
          </div>
        )}

      </div>

      {cartItems.length ? (
        <div className='mt-3 flex justify-between items-center p-3 rounded-md bg-gray-50 '>
          <button onClick={() => setShow(false)} className='px-6 py-4 bg-[#0e2447] text-white md:text-[18px] text-[16px]'>
           Continuer les achats
          </button>
        </div>
      ) : ''}
    </motion.div>
  );
}

export default Cart;
