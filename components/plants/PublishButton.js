import React, { useState } from 'react';
import { getStorage, ref, uploadBytes, getDownloadURL } from 'firebase/storage';
import { storage } from '../../firebase-config';

const PublishButton = ({ onImageUpload }) => {
  const [isPublishing, setIsPublishing] = useState(false);

  const handlePublish = async (event) => {
    setIsPublishing(true);

    const file = event.target.files[0]; 

    if (!file) {
      console.error('Aucun fichier sélectionné');
      setIsPublishing(false);
      return;
    }

    const storageRef = ref(storage, `images/${file.name}`);

    try {
      await uploadBytes(storageRef, file);
      console.log('Fichier téléchargé avec succès');

      const downloadURL = await getDownloadURL(storageRef);

      onImageUpload(downloadURL);
    } catch (error) {
      console.error('Erreur lors du téléchargement du fichier:', error);
    }

    setIsPublishing(false);
  };

  return (
    <label className="bg-[#0e2447] text-white font-bold py-2 px-4 rounded cursor-pointer">
      {isPublishing ? 'Publication en cours...' : 'Publier des fichiers'}
      <input
        type="file"
        accept="image/*"
        style={{ display: 'none' }} 
        onChange={handlePublish}
        disabled={isPublishing} 
      />
    </label>
  );
};

export default PublishButton;
