import React, { useState, useEffect } from 'react';
import PublishButton from '../plants/PublishButton';
import { ref, listAll, getDownloadURL } from 'firebase/storage';
import { storage } from '../../firebase-config'; // Importez la configuration de Firebase Storage
import PlantCard from '../PlantCard';

const Shop = () => {
  const [filters, setFilters] = useState({});
  const [search, setSearch] = useState('');
  const [showFilter, setShowFilter] = useState(false);
  const [imageUrls, setImageUrls] = useState([]);

  useEffect(() => {
    const storageRef = ref(storage, 'images');
    listAll(storageRef)
      .then((res) => {
        Promise.all(res.items.map(item => getDownloadURL(item))).then(urls => {
          setImageUrls(urls);
        });
      })
      .catch((error) => {
        console.error('Erreur lors de la récupération des URLs des images:', error);
      });
  }, []);
  
  const takeFilters = (filters) => {
    setFilters(filters);
  }

  const takeSearch = (search) => {
    setSearch(search);
  }

  return (
    <div>
      <div className="flex justify-center items-center mb-[6rem] mt-[6rem] space-x-6">
        <PublishButton />
      </div>
      <div className="flex justify-center items-center">
        {imageUrls.map((url, index) => (
          <img key={index} src={url} alt={`Image ${index}`} style={{ width: '200px', height: '200px', margin: '10px' }} />
        ))}
      </div>
      <PlantCard />
    </div>
  );
}

export default Shop;