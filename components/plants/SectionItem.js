import Link from "next/link";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { doc, updateDoc } from "firebase/firestore";
import { db } from "../../firebase-config";
import { cartActions } from "../../slices/cartSlice";
import { uiActions } from "../../slices/uiSlice";
import { userActions } from "../../slices/userSlice";
import { addLove, retrieveLove } from "../../slices/itemsSlice";
import PlantCard from "./PlantCard";

function SectionItem({ title, mainPic, type, price, id }) {
  const dispatch = useDispatch();
  const {
    connected: userConnected,
    user: { docId: userUid, wishlist: userWish },
  } = useSelector((state) => state.user);
  const item = useSelector((state) => state.items.itemsItems).find(
    (item) => item.id == id,
  );

  async function addToCart(event) {
    event.stopPropagation();
    dispatch(
      cartActions.ADD_TO_CART({
        id,
        title,
        price,
        quantity: 1,
        total: price,
        mainPic,
      }),
    );
  }

  async function addToWish(e) {
    e.stopPropagation();
    if (userConnected) {
      const itemRefrence = doc(db, "users", userUid);
      const isItemInWishlist = userWish.find((item) => parseInt(item) == id);
      const updatedWishlist = isItemInWishlist
        ? userWish.filter((item) => parseInt(item) != id)
        : [...userWish, id];

      dispatch(userActions.ADD_TO_WISH(id));
      await updateDoc(itemRefrence, { wishlist: updatedWishlist });
      await dispatch(
        isItemInWishlist
          ? retrieveLove(id, item.uid, item.loves)
          : addLove(id, item.uid, item.loves),
      );
    } else {
      dispatch(uiActions.showProfile());
    }
  }

  return (
    <div className="flex items-center justify-center">
      <PlantCard
        title={title}
        mainPic={mainPic}
        type={type}
        price={price}
        id={id}
        addToCart={addToCart}
        addToWish={addToWish}
      />
    </div>
  );
}

export default SectionItem;
