import React, { useState } from 'react';
import { deleteObject, refFromURL } from 'firebase/storage';
import { storage } from '../../firebase-config'; // Importez la configuration de Firebase Storage

const DeleteButton = ({ selectedImages, onImageDelete }) => {
  const [isDeleting, setIsDeleting] = useState(false);

  const handleDelete = async () => {
    setIsDeleting(true); // Mettre à jour l'état pour indiquer que la suppression est en cours

    try {
      // Parcourir toutes les images sélectionnées pour les supprimer
      await Promise.all(selectedImages.map(async imageUrl => {
        const imageRef = refFromURL(storage, imageUrl);
        await deleteObject(imageRef);
        onImageDelete(imageUrl);
      }));
    } catch (error) {
      console.error('Erreur lors de la suppression des images:', error);
    }

    setIsDeleting(false); // Réinitialiser l'état après la suppression des images
  };

  return (
    <button className="bg-[#FF0000] text-white font-bold py-2 px-4 rounded cursor-pointer delete-button" onClick={handleDelete} disabled={isDeleting || selectedImages.length === 0}>
      {isDeleting ? 'Suppression en cours...' : 'Supprimer'}
    </button>
  );
};

export default DeleteButton;
