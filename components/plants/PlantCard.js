import Link from "next/link";
const PlantCard = ({
  id,
  title,
  type,
  mainPic,
  price,
  addToCart,
  addToWish,
  userWish,
  userConnected,
}) => {
  const Button = ({ onClick, children }) => (
    <button
      className="flex-auto transition hover:bg-[#7D916C] md:text-[#7D916C] text-white bg-[#7D916C] md:bg-white hover:text-white border md:border-[#7D916C] py-2 text-[14px] md:text-[16px]"
      onClick={onClick}
    >
      {children}
    </button>
  );

  const HeartIcon = ({ onClick, filled }) => (
    <img
      onClick={onClick}
      className="w-7"
      src={
        filled
          ? "/icons/heart-icon-black-filled.svg"
          : "/icons/heart-icon-black.svg"
      }
      alt=""
    />
  );

  return (
    <Link href="/plants/[id]" as={`/plants/${id}`}>
      <div className="flex flex-col items-center justify-center p-2 w-[250px] h-[400px] rounded-md border border-gray-200 transition text-black hover:bg-gray-50">
        <img
          src={mainPic}
          alt=""
          className="w-[200px] h-[200px] object-cover"
        />
        <div className="flex justify-between items-center w-full mt-4">
          <div>
            <h1 className="text-lg md:text-2xl -mb-1 group-hover:text-[#7D916C] transition">
              {title}
            </h1>
            <p className="text-sm md:text-lg text-gray-400">{type}</p>
          </div>
          <p className="md:text-xl text-sm text-white px-2 py-3 bg-[#7D916C]">
            {price}€
          </p>
        </div>
        <div className="flex gap-5 mt-4 items-center w-full">
          <Button onClick={addToCart}>Ajouter au panier</Button>
          <div className="rounded-full p-2 transition hover:bg-[#7c7c7c2c] cursor-pointer">
            <HeartIcon
              onClick={addToWish}
              filled={
                userConnected && userWish.find((item) => parseInt(item) == id)
              }
            />
          </div>
        </div>
      </div>
    </Link>
  );
};

export default PlantCard;
