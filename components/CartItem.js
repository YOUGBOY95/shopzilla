import React, { useState } from "react";
import { motion } from "framer-motion";
import { useDispatch } from 'react-redux';
import { REMOVE_CART_ITEM, SET_ITEM_QUANTITY } from '../slices/cartSlice'; // Importer les actions de suppression et de modification de la quantité d'éléments du panier

function CartItem({ id, title, price, quantity, mainPic }) {
  const dispatch = useDispatch();
  const [itemQuantity, setItemQuantity] = useState(quantity);

  function handleRemoveFromCart() {
    dispatch(REMOVE_CART_ITEM(id)); // Utiliser l'action de suppression des éléments du panier
  }

  function increaseQuantity() {
    const newQuantity = itemQuantity + 1;
    setItemQuantity(newQuantity);
    dispatch(SET_ITEM_QUANTITY({ id, quantity: newQuantity }));
  }

  function decreaseQuantity() {
    if (itemQuantity > 1) {
      const newQuantity = itemQuantity - 1;
      setItemQuantity(newQuantity);
      dispatch(SET_ITEM_QUANTITY({ id, quantity: newQuantity }));
    }
  }

  return (
    <motion.div className="flex w-full py-2 px-2 hover:bg-gray-50 rounded-md">
      <div className="flex flex-1">
        <img src={mainPic} className="md:w-32 w-32" alt={title} />
        <div className="pl-2 md:pl-4 flex flex-col md:justify-around justify-start">
          <div>
            <h1 className="text-xl md:text-2xl -mb-2">{title}</h1>
            <p className="text-lg text-gray-400">Plant</p>
            <p className="text-lg mb-5 text-[#b5c4a8] md:block hidden">{price}€</p>
          </div>

          <label htmlFor="quantity" className="md:block hidden">
            Quantity
            <button onClick={decreaseQuantity} className="bg-gray-50 focus:outline-none py-1 w-8 ml-2 border-r-gray-200 border-r hover:bg-gray-200">
              -
            </button>
            <input
              max="99"
              type="number"
              className="bg-gray-50 focus:outline-none py-1 px-2 w-10 text-center"
              value={itemQuantity}
              readOnly
            />
            <button onClick={increaseQuantity} className="bg-gray-50 focus:outline-none py-1 w-8 border-l-gray-200 border-l hover:bg-gray-200">
              +
            </button>
          </label>
        </div>
      </div>
      <div className="flex flex-col justify-between items-end">
        <button onClick={handleRemoveFromCart} className="transition hover:bg-[#7c7c7c2c] p-3 rounded-sm"><img src="/icons/trash-icon-black.svg" className="w-7" alt="Supprimer" /></button>
        <p className="ml-10 text-lg">total : <span className=" font-normal">{(price * itemQuantity).toFixed(2)} €</span></p>
      </div>
    </motion.div>
  );
}

export default CartItem;
