import React from 'react'
function Footer() {


  return (
    <div className='bg-[#ffffff5e] md:flex items-stretch relative overflow-hidden px-40 pt-5 pb-5 hidden'>
        <div className='px-10 flex-1 flex flex-col items-start justify-start'>
            <img className='w-40 -ml-7' src="/logoTwo.svg" alt="" />
        </div>
        <div className='flex flex-col px-10 border-l border-[#0e2447] justify-around flex-1 items-center'>
            <div className='flex gap-3'>
                <p className='opacity-100 text-black'>©SHOPZILLA</p>
            </div>

        </div>

    </div>
  )
}

export default Footer