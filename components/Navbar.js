import React, { useState, useEffect, useCallback } from "react";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { uiActions } from "../slices/uiSlice";

const renderNavItem = (path, label, router, scrolled) => (
  <li
    className={`relative group ${scrolled ? "text-black" : ""} hover:opacity-100 ${router.pathname === path ? "opacity-100 font-normal" : "opacity-70"}`}
  >
    <Link href={path}>{label}</Link>
    <span
      className={`${router.pathname === path ? "w-1/2" : "w-0"} h-[1px] ${scrolled ? "bg-black" : "bg-white"} absolute top-[26px] left-0 group-hover:w-1/2 transition-all`}
    ></span>
  </li>
);

const renderIcon = (action, icon, scrolled) => (
  <div
    onClick={action}
    className={`w-11 h-11 border-[0.7px] rounded-full p-3 ${scrolled ? "hover:bg-[#7c7c7c2c]" : "hover:bg-[#ffffff2c]"} transition cursor-pointer`}
  >
    <img
      className="w-7"
      src={scrolled ? `/icons/${icon}-black.svg` : `/icons/${icon}.svg`}
      alt=""
    />
  </div>
);

function Navbar() {
  const router = useRouter();
  const [itemAdded, setItemAdded] = useState(false);
  const cartCount = useSelector((state) => state.cart.cartItems.length);
  const [scrolled, setScrolled] = useState(false);
  const dispatch = useDispatch();

  const showProfile = useCallback(() => dispatch(uiActions.showProfile()), [dispatch]);
  const showCart = useCallback(() => dispatch(uiActions.showCart()), [dispatch]);

  useEffect(() => {
    const changeBackground = () => {
      setScrolled(window.scrollY >= 100 || router.pathname !== "/");
    };

    window.addEventListener("scroll", changeBackground);
    changeBackground();

    return () => {
      window.removeEventListener("scroll", changeBackground);
    };
  }, [router.pathname]);

  const renderNavItem = (path, label) => (
    <li className={`relative group ${scrolled ? 'text-black' : ''} hover:opacity-100 ${router.pathname === path ? 'opacity-100 font-normal' : 'opacity-70'}`}>
      <Link href={path}>{label}</Link>
      <span className={`${router.pathname === path ? 'w-1/2' : 'w-0'} h-[1px] ${scrolled ? 'bg-black' : 'bg-white'} absolute top-[26px] left-0 group-hover:w-1/2 transition-all`}></span>
    </li>
  );

  const renderIcon = (action, icon) => (
    <div onClick={action} className={`w-11 h-11 border-[0.7px] rounded-full p-3 ${scrolled ? 'hover:bg-[#7c7c7c2c]' : 'hover:bg-[#ffffff2c]'} transition cursor-pointer`}>
      <img className="w-7" src={scrolled ? `/icons/${icon}-black.svg` : `/icons/${icon}.svg`} alt="" />
    </div>
  );

  return (
    <nav
      className={`flex  items-center justify-between  md:px-48 px-5 fixed top-0 w-full z-20 ${scrolled ? "bg-[#ffffff] shadow-md" : ""} transition-all h-16`}
    >
      <div className="flex items-center">
        <ul className="md:flex gap-16  text-[18px] tracking-widest hidden">
          {renderNavItem("/", "Accueil", router, scrolled )}
          {renderNavItem("/plants", "Shop", router, scrolled)}
        </ul>
      </div>
      <div className="flex gap-5 items-center">
        <div className="w-80 relative seconde:block hidden"></div>
        {renderIcon(() => dispatch(uiActions.showProfile()), 'user-icon')}
        <div className="relative">
          {renderIcon(() => dispatch(uiActions.showCart()), 'cart-icon')}
          {cartCount != 0 && <span className={`absolute text-md -top-[5px] -right-[8px] ${scrolled ? 'bg-[#7D916C]' : 'bg-[#ffffff48]'} ${!scrolled ? 'border border-[0.7px]' : ''} text-white rounded-full h-[22px] w-[22px] flex justify-center items-center font-normal`}>{cartCount}</span>}
          {itemAdded && <span className={`animate-ping absolute text-md -top-[5px] -right-[8px] ${scrolled ? 'bg-[#7D916C]' : 'bg-[#ffffff48]'} ${scrolled ? '' : 'border border-[0.7px] '} text-white rounded-full h-[22px] w-[22px] flex justify-center items-center font-normal`}>{cartCount}</span>}
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
