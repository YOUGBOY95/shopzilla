import React from "react";
import { useDispatch } from 'react-redux';
import { ADD_TO_CART } from '../slices/cartSlice';
import CartItem from './CartItem';

function PlantCard() {
    const dispatch = useDispatch();

    // Fonction pour ajouter un article au panier
    function addToCart(item) {
        dispatch(ADD_TO_CART({
            id: item.id,
            title: item.nom,
            price: parseFloat(item.prix),
            quantity: 1,
            total: parseFloat(item.prix),
            mainPic: item.image
        }));
    }

    const plantData = [
        {
            nom: "Pippa",
            espece: "Peace Lily",
            image: "https://res.cloudinary.com/patch-gardens/image/upload/c_fill,f_auto,h_840,q_auto:good,w_840/v1564153137/products/peace-lily-1530c2.jpg",
            prix: "12.99",
        },
        {
            nom: "Aggie",
            espece: "Red Chinese Evergreen",
            image: "https://res.cloudinary.com/patch-gardens/image/upload/c_fill,f_auto,h_840,q_auto:good,w_840/v1618408664/lhur6din2mgxsoywyvra.jpg",
            prix: "15.99",
        },
        {
            nom: "Susie",
            espece: "Snake Plant",
            image: "https://res.cloudinary.com/patch-gardens/image/upload/c_fill,f_auto,h_840,q_auto:good,w_840/v1563812091/products/snake-plant-e0fb21.jpg",
            prix: "9.99",
        },
        {
            nom: "Chris",
            espece: "Curly Spider Plant",
            image: "https://res.cloudinary.com/patch-gardens/image/upload/c_fill,f_auto,h_840,q_auto:good,w_840/v1539776219/products/chlorophytum-comosum-bonnie-1f3b6c.jpg",
            prix: "16.99",
        },
        {
            nom: "Lyla",
            espece: "Succulent",
            image: "https://res.cloudinary.com/patch-gardens/image/upload/c_fill,f_auto,h_840,q_auto:good,w_840/v1630666435/wfrjb0ivgo7u0z5qluyu.jpg",
            prix: "7.99",
        }       
    ];

    return (
        <div className="grid grid-cols-3 gap-4 mx-auto max-w-screen-lg">
            {plantData.map((item, index) => (
                <div className="bg-gray-800 bg-opacity-20 hover:bg-opacity-25 transition p-4 w-80 text-white shadow items-center space-y-2 rounded-xl" key={index}>
                    <img src={item.image} alt={item.nom} className="rounded-xl"/>
                    <h2 className="text-2xl font-bold text-black">{item.nom}</h2>
                    <h2 className="text-l font-normal opacity-50 text-black">{item.espece}</h2>
                    <button onClick={() => addToCart(item)} className="flex-auto transition p-2 rounded-lg bg-[#0e2447] hover:bg-[#0e2447] text-white border border-[#7D916C] py-5 text-[18px]">
                        Ajouter au panier
                    </button>
                    <p className="text-2xl italic font-light bg-[#0e2447] float-end p-2 rounded-lg">{item.prix}€</p>
                </div>
            ))}
        </div>
    );
}

export default PlantCard;
