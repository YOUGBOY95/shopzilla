import Link from "next/link";
import React from "react";
import PlantSlider from "./PlantSlider";

function Homepage() {
  return (
    <section className="min-h-screen w-full bg-[url(https://i.ibb.co/cgyY7sr/background.jpghttps://i.ibb.co/cgyY7sr/background.jpg)] bg-cover bg-no-repeat bg-center relative z-10 flex items-center md:px-36 px-5 pt-20 pb-10">
      <div className="absolute top-0 bottom-0 left-0 right-0 bg-[#0e2447] opacity-[0.65]"></div>
      <div className="z-10 flex justify-around w-full  items-center">
        <div>
          <h1 className="seconde:text-6xl md:text-4xl text-5xl  mb-10">
            Bienvenue sur Shopzilla !
          </h1>
          <p className="text-2xl 3xl:text-xl text-gray-300  mb-14">
            Cultivez la vie, plantez le bonheur !
          </p>
          <Link href={{ pathname: "/plants" }}>
            <button className=" transition hover:bg-white hover:text-black text-white py-3 px-6 rounded-full text-[20px] border-[0.5px]">
              Voir tout
            </button>
          </Link>
        </div>
        <div>
          <PlantSlider />
        </div>
      </div>
    </section>
  );
}

export default Homepage;
