import React from "react";

function WhyPlants() {
  return (
    <section className="bg-[#0e2447] md:px-56 px-5 pb-16 relative overflow-hidden">
      <h1 className="text-3xl pt-16 mb-16 md:pl-28">Que proposons-nous ?</h1>
      <div className="flex flex-col md:flex-row md:gap-0 gap-10 md:justify-around md:items-center">
        <div className=" relative w-72 h-72 rounded-full">
          <img
            className="absolute top-0 left-0 rounded-full"
            src="https://res.cloudinary.com/patch-gardens/image/upload/c_fill,f_auto,h_840,q_auto:good,w_840/v1563876772/products/snake-plant-dda326.jpg"
            alt=""
            width="300"
            height="300"
          />
        </div>
        <p className="md:w-1/2 text-[18px] md:leading-10 text-gray-200 font-serif">
          {" "}
          "Notre site vous propose une
          expérience fluide et sécurisée pour acquérir ou céder des plantes et
          services associés. Trouvez facilement ce que vous recherchez en
          quelques clics ou mettez en avant vos végétaux devant un public
          élargi. Rejoignez-nous dès maintenant pour une expérience
          d'horticulture en ligne simplifiée."{" "}
        </p>
      </div>
    </section>
  );
}

export default WhyPlants;
