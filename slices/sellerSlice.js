import { createSlice } from "@reduxjs/toolkit";
import { signInWithEmailAndPassword, signOut } from "firebase/auth";
import { collection, query, where, getDocs } from "firebase/firestore";
import { auth, db, storage } from "../firebase-config";
import cartSlice from "./cartSlice";
import { getStorage, ref, uploadBytes } from "firebase/storage";
export function signIn(email, password) {
  return async (dispatch) => {
    signInWithEmailAndPassword(auth, email, password)
      .then(async (userCredential) => {
        const user = userCredential.user;

        const q = query(collection(db, "sellers"), where("uid", "==", user.uid));
        const userInfos = await getDocs(q);
        userInfos.forEach((doc) => {
          dispatch(sellerSlice.actions.SIGN_IN({ ...doc.data(), docId: doc.id }));

          const localData = JSON.stringify({ email: email, password: password });
          localStorage.setItem("account", localData);
        });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        dispatch(sellerSlice.actions.SET_ERROR(errorCode));
      });
  };
}

export function signOutSeller() {
  return (dispatch) => {
    signOut(auth)
      .then(() => {
        dispatch(sellerSlice.actions.SIGN_OUT());
      })
      .catch((error) => {
        // An error happened.
      });
  };
}

export function addPhotoToItem(itemId, photoFile) {
  return async (dispatch) => {
    try {
      const storageRef = getStorage(storage);
      const itemPhotosRef = ref(storageRef, `itemPhotos/${itemId}/${photoFile.name}`);

      await uploadBytes(itemPhotosRef, photoFile);

      const photoURL = await getDownloadURL(itemPhotosRef);

      dispatch(sellerSlice.actions.ADD_PHOTO_TO_ITEM({ itemId, photoURL }));

      return photoURL;
    } catch (error) {
      console.error("Error uploading photo:", error);
      throw error;
    }
  };
}

const initialState = {
  connected: false,
  connectionNote: false,
  connectionError: "",
  seller: {
    username: "",
    uid: "",
    items: [],
  },
};

const sellerSlice = createSlice({
  name: "seller",
  initialState,
  reducers: {
    SIGN_IN(state, { payload }) {
      state.connected = true;
      state.connectionNote = true;
      state.seller = { ...payload };
    },
    SET_ERROR(state, { payload }) {
      state.connectionError = payload;
    },
    SIGN_OUT(state) {
      state.connected = false;
      state.seller = {
        username: "",
        uid: "",
      };
    },
    ADD_ITEM(state, { payload }) {
      state.seller.items.push(payload);
    },

    ADD_PHOTO_TO_ITEM(state, { payload }) {
      state.seller.items.find(item => item.id === payload.itemId).photoURL = payload.photoURL;
    },
  },
});

export default sellerSlice.reducer;
export const sellerActions = sellerSlice.actions;
