import { createSlice } from "@reduxjs/toolkit";

const cartInitialState = {
  cartItems: []
};

const cartSlice = createSlice({
  name: 'cart',
  initialState: cartInitialState,
  reducers: {
    GET_CART_ITEMS(state) {
      const cartData = localStorage.getItem('cart');
      if (cartData) {
        try {
          state.cartItems = JSON.parse(cartData);
        } catch (error) {
          console.error("Error parsing cart data:", error);
        }
      }
    },
    ADD_TO_CART(state, action) {
      const { id, title, price, quantity, total, mainPic } = action.payload;
      const existingItemIndex = state.cartItems.findIndex(item => item.id === id);
      if (existingItemIndex !== -1) {
        state.cartItems[existingItemIndex].quantity += quantity;
        state.cartItems[existingItemIndex].total += total;
      } else {
        state.cartItems.push({ id, title, price, quantity, total, mainPic });
      }
      localStorage.setItem('cart', JSON.stringify(state.cartItems));
    },
    SET_ITEM_QUANTITY(state, action) {
      const { id, quantity } = action.payload;
      const existingItem = state.cartItems.find(item => item.id === id);
      if (existingItem) {
        existingItem.quantity = quantity;
        existingItem.total = existingItem.price * quantity;
      }
      localStorage.setItem('cart', JSON.stringify(state.cartItems));
    },
    REMOVE_CART_ITEM(state, action) {
      const idToRemove = action.payload;
      state.cartItems = state.cartItems.filter(item => item.id !== idToRemove);
      localStorage.setItem('cart', JSON.stringify(state.cartItems));
    }
  }
});

export default cartSlice.reducer;

export const { GET_CART_ITEMS, ADD_TO_CART, SET_ITEM_QUANTITY, REMOVE_CART_ITEM } = cartSlice.actions;
